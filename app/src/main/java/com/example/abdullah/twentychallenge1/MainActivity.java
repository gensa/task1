package com.example.abdullah.twentychallenge1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
    final float ZOOM_LEVEL = 11;
    GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("الخريطة");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        LatLng saudUni, nouraUni;
        saudUni = new LatLng(24.722575, 46.627601);
        nouraUni = new LatLng(24.852394, 46.723890);
        insertMarker(saudUni, "King saud university");
        insertMarker(nouraUni, "Princess Nora bint Abdul Rahman University");
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(saudUni, ZOOM_LEVEL));
    }

    private void insertMarker(LatLng nouraUni, String title) {
        map.addMarker(new MarkerOptions().position(nouraUni).title(title).icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker)));
    }
}
